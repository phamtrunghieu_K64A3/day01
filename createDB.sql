CREATE DATABASE qlsv;

CREATE TABLE dmkhoa (
  MaKH varchar(6) NOT NULL PRIMARY KEY,
  TenKhoa varchar(30)
);

CREATE TABLE sinhvien (
  MaSV varchar(6) NOT NULL PRIMARY KEY,
  HoSV varchar(30),
  TenSV varchar(15),
  GioiTinh char(1),
  NgaySinh datetime,
  NoiSinh varchar(50),
  DiaChi varchar(50),
  MaKH varchar(6),
  HocBong int
);